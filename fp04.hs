module Fp04 where

import Data.Function (fix, on)

x = 2 -- глобальное

y = 42 -- глобальное

foo =
  let z = x + y -- глобальное (foo) локальное (z)
   in print z -- отступ (layout rule)

add x y = x + y -- определение add

add' x y = x + y

add'' = \x y -> x + y

fortyTwo = add 40 2 -- вызов add

--oops = print add 1 2
good = print (add 1 2)

z = 1 -- ok, связали
--z = 2         -- ошибка

q q = \q -> q -- ok, но...

factorial n =
  if n > 1
    then n * factorial (n -1)
    else 1

factorial' n = helper 1 n

helper acc n =
  if n > 1
    then helper (acc * n) (n - 1)
    else acc

factorial'' n' = helper 1 n'
  where
    helper acc n =
      if n > 1
        then helper (acc * n) (n - 1)
        else acc

factorial''' n' =
  let helper acc n
        | n > 1 = helper (acc * n) (n - 1)
        | otherwise = acc
   in helper 1 n'

mult :: Integer -> (Integer -> Integer)
mult x1 x2 = x1 * x2

-- Операторы

a *+* b = a * a + b * b

res = 3 *+* 4

(**+**) a b = a * a * a + b * b * b

res1 = (**+**) 2 3

res2 = 2 **+** 3

x `plus` y = x + y

res3 = 2 `plus` 3

res4 = plus 2 3

infixl 6 *+*, **+**, `plus`

f0 = 39 + 3

f1 x = x + 3

res5 = f1 39

f2 = \x -> x + 3

f3 = (+ 3)

triple :: Num a => a -> a
triple x = x * 3

sign :: (Ord a, Num a, Num p) => a -> p
sign x
  | x > 0 = 1
  | x == 0 = 0
  | otherwise = -1

nor :: Bool -> Bool -> Bool
nor x y = not (x || y)

nand :: Bool -> Bool -> Bool
nand x y = not (x && y)

testBool :: (Bool -> Bool -> Bool) -> [Bool]
testBool f =
  let bs = [False, True]
   in do
        b1 <- bs
        f b1 <$> bs

fib :: Integer -> Integer
fib = helper 0 1
  where
    helper a0 a1 n
      | n == 0 = a0
      | n == 1 = a1
      | otherwise = helper a1 (a0 + a1) (n - 1)

myflip :: (a -> b -> c) -> b -> a -> c
myflip f x y = f y x

fixedFact :: Integer -> Integer
fixedFact = fix (\rec n -> if n > 1 then n * rec (n - 1) else 1)

sumSqrs :: Integer -> Integer -> Integer
sumSqrs = (+) `on` (^ 2)

fac :: (Ord b, Num b) => b -> b
fac n = fst $ until (\x -> snd x >= n) ss zz
  where
    zz = (1, 0)
    ss p = (f * (s + 1), s + 1)
      where
        f = fst p
        s = snd p

-- >>> fac 5
-- 120

untilFib :: Integer -> Integer
untilFib n = first $ until (\x -> third x <= 0) ss zz
  where
    first = \(x, _, _) -> x
    third = \(_, _, x) -> x
    zz = (0, 1, n)
    ss (a0, a1, count) = (a1, a0 + a1, count - 1)

-- >>> map untilFib [0..8]
-- [0,1,1,2,3,5,8,13,21]

doubleFact :: Integer -> Integer
doubleFact = helper 1
  where
    helper acc n
      | n > 1 = helper (acc * n) (n - 2)
      | otherwise = acc

-- >>> doubleFact (-2)
-- 1

tripleSeq :: Integer -> Integer
tripleSeq = helper 1 2 3
  where
    helper a0 a1 a2 n
      | n == 0 = a0
      | n == 1 = a1
      | n == 2 = a2
      | otherwise = helper a1 a2 (a2 + a1 - (2 * a0)) (n - 1)

-- >>> map tripleSeq [0..15]
-- [1,2,3,3,2,-1,-5,-10,-13,-13,-6,7,27,46,59,51]

fibonacci :: Integer -> Integer
fibonacci n
  | n >= 0 = fib n
  | otherwise = - (fib $ abs n)

-- >>> map fibonacci [-8..8]
-- [-21,-13,-8,-5,-3,-2,-1,-1,0,1,1,2,3,5,8,13,21]


sum'n'count :: Integer -> (Integer, Integer)
sum'n'count = foldl combine (0, 0) . digits 10 . abs
  where
    combine (sum, count) d = (sum + d, count + 1)
    digits base a = helper [] a
      where helper ds x
              | x > 0 = helper (rem x base : ds) (quot x base)
              | otherwise = ds

-- >>> sum'n'count 999
-- (27,3)