{-# LANGUAGE StandaloneDeriving, FlexibleContexts, UndecidableInstances #-}
module Fp13 where

data Mu f = In (f (Mu f))
-- ср. fix f = f (fix f)
deriving instance Show (f (Mu f)) => Show (Mu f)

{-
> :k Mu
Mu :: (* -> *) -> *
> :t In
In :: f (Mu f) -> Mu f
-}


-- data Nat = Z | S Nat

-- функтор, описывающий структуру типа
-- N = \X. 1 + X
data N x = Z | S x deriving Show

instance Functor N where
  fmap g  Z    = Z
  fmap g (S x) = S (g x)

-- рекурсивный тип вводим через неподвижную точку функтора на уровне типов
type Nat = Mu N

{-
нерекурсивный функтор
> :t Z
Z :: N x
> :t S Z
S Z :: N (N x)
> :t S (S Z)
S $ S Z :: N (N (N x))
тип Nat, то есть Mu N, как его неподвижная точка
> :t In Z
In Z :: Mu N
> :t In (S (In Z))
In (S (In Z)) :: Mu N
> :t In (S (In (S (In Z))))
In (S (In (S (In Z)))) :: Mu N
-}


-- data List a = Nil | Cons a (List a)

-- функтор, описывающий структуру типа
-- L = \A. \X. 1 + A * X
data L a l = Nil | Cons a l deriving Show

instance Functor (L a) where
 fmap g Nil        = Nil
 fmap g (Cons a l) = Cons a (g l)

-- рекурсивный тип вводим через неподвижную точку функтора на уровне типов
type List a = Mu (L a) 

{-
-- нерекурсивный функтор
Nil                     :: L a l
Cons 'i' Nil            :: L Char (L a l)
Cons 'h' $ Cons 'i' Nil :: L Char (L Char (L a l))
тип List Char, то есть Mu (L Char), как его неподвижная точка
In Nil                                 :: Mu (L a)
In $ Cons 'i' $ In Nil                 :: Mu (L Char)
In $ Cons 'h' $ In $ Cons 'i' $ In Nil :: Mu (L Char)
-}

-----------------------------------------------------------------

-- Рассмотрим преобразование 
copy :: Functor f => Mu f -> Mu f
copy (In x) = In $ fmap copy x

-- Утверждение: это преобразование есть тождество.

{-
Напишем обобщение copy, которое заменяет In :: f (Mu f) -> Mu f не на себя же, 
а на произвольную phi :: f a -> a.
Получим обобщение понятия свёртки, катаморфизм:
-}

cata :: Functor f => (f a -> a) -> Mu f -> a
cata phi (In x) = phi $ fmap (cata phi) x

{-
Для данных функтора f и типа a функция phi :: f a -> a известна как f-алгебра.
-}

-- Если в примере с функтором N задать функцию phi, заменяющую Z на 0, а S на succ, 
-- то получим N-алгебру типа N Int -> Int
phiN :: N Int -> Int
phiN Z     = 0
phiN (S n) = succ n

-- Применяя cata к этой алгебре, получим стандартный преобразователь 
natToInt :: Nat -> Int -- алиас для Mu N -> Int
natToInt = cata phiN

{-
>  natToInt $ In (S (In (S (In Z))))
2
-}

-- Пример (L a)-алгебры
phiL :: L a [a] -> [a]
phiL Nil         = []
phiL (Cons e es) = e : es

listify :: List a -> [a] -- алиас для Mu (L a) -> [a]
listify = cata phiL

{-
>  listify $ In Nil
[]
>  listify $ In $ Cons 'i' $ In Nil
"i"
>  listify $ In $ Cons 'h' $ In $ Cons 'i' $ In Nil
"hi"
-}

-- Ещё пара списочных алгебр

phiLLen :: L a Int -> Int
phiLLen Nil         = 0
phiLLen (Cons _ es) = 1 + es

phiLSum :: Num a => L a a -> a
phiLSum Nil         = 0
phiLSum (Cons e es) = e + es


{-
> cata phiLLen $ In $ Cons 'h' $ In $ Cons 'i' $ In Nil
2
> cata phiLSum $ In $ Cons 2 $ In $ Cons 3 $ In Nil
5
-}


----------------------------------------------------------------------

-- Анаморфизм

-- Введём операцию, обратную In
out :: Mu f -> f (Mu f)
out (In x) = x

-- Рассмотрим преобразование рекурсивного типа в себя
copy' :: Functor f => Mu f -> Mu f
copy' x = In $ fmap copy' (out x)

-- Утверждение: это преобразование есть тождество.

{-
--  Покажем что copy' = id на примере выражения In (S (In (S (In Z)))) типа Nat:
copy'               (In (S (In (S (In Z)))))   ~> по def copy'
In (fmap copy' (out (In (S (In (S (In Z))))))  ~> по def out
In (fmap copy'          (S (In (S (In Z))))    ~> по def fmap
In (S (copy'               (In (S (In Z)))))   ~> по def copy'
In (S (In (fmap copy' (out (In (S (In Z))))))) ~> по def out
In (S (In (fmap copy'          (S (In Z)))))   ~> по def fmap
In (S (In (S (copy'               (In Z)))))   ~> по def copy'
In (S (In (S (In (fmap copy'(out  (In Z))))))) ~> по def out
In (S (In (S (In (fmap copy'          Z)))))   ~> по def fmap
In (S (In (S (In Z)))))
-}

-- Заменим в copy' каждое вхождение out :: Mu f -> f (Mu f) 
-- на произвольную функцию psi :: a -> f a 
-- Получим нотацию анаморфизма
ana :: Functor f => (a -> f a) -> a -> Mu f
ana psi x = In $ fmap (ana psi) (psi x)

-- Пример 

psiN :: Int -> N Int
psiN 0 = Z
psiN n = S (n-1)

-- задает анаморфизм
intToNat :: Int -> Nat
intToNat = ana psiN

{-
> intToNat 3
In (S (In (S (In (S (In Z))))))
-}
