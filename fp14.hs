module Fp14 where

insertAfter5 :: Int -> [Int] -> [Int]
insertAfter5 v xs  = helper [] xs where
  helper _ [] = []
  helper hs (e : ts) 
    | e == 5    = hs ++ e : v : ts
    | otherwise = helper (hs ++ [e]) ts
{-
> insertAfter5 42 [1..10]
[1,2,3,4,5,42,6,7,8,9,10]
-}

-- Зиппер для списка
type LZ a = (
             [a] -- хвост
            ,[a] -- контекст
            )

-- вкладываем список в зиппер
mklz :: [a] -> LZ a
mklz xs = (xs, [])
{-
> let lzp = mklz [0..3]
> lzp
([0,1,2,3],[])
-}

-- вынимаем список из зиппера
unlz :: LZ a -> [a]
unlz (xs, []) = xs
unlz (xs, y:ys) = unlz (y:xs, ys)

-- навигация вперёд: контекст нарастает, хвост облезает
fwd :: LZ a -> LZ a
fwd (x:xs, ys) = (xs, x:ys)
{-
> fwd lzp
([1,2,3],[0])
> (fwd . fwd) lzp
([2,3],[1,0])
> (fwd . fwd . fwd) lzp
([3],[2,1,0])
> (fwd . fwd . fwd . fwd) lzp
([],[3,2,1,0])
> (fwd . fwd . fwd . fwd . fwd) lzp
*** Exception: Non-exhaustive patterns in function fwd
-}

-- навигация назад: контекст сокращается, хвост нарастает
bck :: LZ a -> LZ a
bck (xs, y:ys) = (y:xs, ys)

eolz :: LZ a -> Bool
eolz ([], _) = True
eolz _ = False

bolz :: LZ a -> Bool
bolz (_, [])  = True
bolz _ = False

-- внесение изменений в значение в фокусе
insertLZ :: a -> LZ a -> LZ a
insertLZ v (xs, ys) = (v:xs, ys)

deleteLZ :: LZ a -> LZ a
deleteLZ (_:xs, ys) = (xs, ys)

updateLZ :: a -> LZ a -> LZ a
updateLZ v (_:xs, ys) = (v:xs, ys)
{-
> updateLZ 42 $ fwd $ fwd zp
([42,3],[1,0])
> unlz $ updateLZ 42 $ fwd $ fwd lzp
[0,1,42,3]
-}


-- Дерево
data Tree a = Empty 
            | Node (Tree a) a (Tree a) 
                deriving (Eq, Show) 

-- Зиппер для дерева
type TZ a = (
             Tree a -- хвост
            ,Ctx a  -- контекст
            )

data Ctx a = Top 
           | L (Ctx a) a (Tree a) 
           | R (Tree a) a (Ctx a)
                deriving (Eq, Show) 

-- вкладываем дерево в зиппер
mktz :: Tree a -> TZ a
mktz t = (t, Top)
{-
> let tr = Node (Node Empty 1 Empty) 2 (Node (Node Empty 3 Empty) 4 (Node Empty 5 Empty))
> let tzp = mktz tr
> tzp
(Node (Node Empty 1 Empty) 2 (Node (Node Empty 3 Empty) 4 (Node Empty 5 Empty)), Top)
-}
   
-- вынимаем дерево из зиппера
untz :: TZ a -> Tree a 
untz (t, Top) = t
untz z        = untz $ up z
   
-- навигация влево: контекст нарастает, дерево усыхает
left :: TZ a -> TZ a
left (Node l x r, c) = (l, L c x r) 

-- навигация вправо: контекст нарастает, дерево усыхает
right :: TZ a -> TZ a
right (Node l x r, c) = (r, R l x c) 
{-
> right tzp
(Node (Node Empty 3 Empty) 4 (Node Empty 5 Empty),R (Node Empty 1 Empty) 2 Top)
> left $ right tzp
(Node Empty 3 Empty,L (R (Node Empty 1 Empty) 2 Top) 4 (Node Empty 5 Empty))
> right $ right tzp
(Node Empty 5 Empty,R (Node Empty 3 Empty) 4 (R (Node Empty 1 Empty) 2 Top))
> left $ right $ right tzp
(Empty,L (R (Node Empty 3 Empty) 4 (R (Node Empty 1 Empty) 2 Top)) 5 Empty)
-}

-- навигация вверх: контекст съёживается, дерево возрождается
up :: TZ a -> TZ a
up (t, L c x r) = (Node t x r, c) 
up (t, R l x c) = (Node l x t, c) 
{-
> up $ right tzp
(Node (Node Empty 1 Empty) 2 (Node (Node Empty 3 Empty) 4 (Node Empty 5 Empty)), Top)
> (up . right) tzp == tzp
True
-}

eotz :: TZ a -> Bool
eotz (Empty, _) = True
eotz _ = False

botz :: TZ a -> Bool
botz (_, Top)  = True
botz _ = False
{-
> let tzp' = left $ right $ right tzp
> tzp'
(Empty,L (R (Node Empty 3 Empty) 4 (R (Node Empty 1 Empty) 2 Top)) 5 Empty)
> eotz tzp'
True
> up $ up $ up tzp'
(Node (Node Empty 1 Empty) 2 (Node (Node Empty 3 Empty) 4 (Node Empty 5 Empty)),
Top)
> botz $ up $ up $ up tzp'
True
-}

updateTZ :: a -> TZ a -> TZ a
updateTZ v (Node l x r, c) = (Node l v r, c)
{-
> tr
Node (Node Empty 1 Empty) 2 (Node (Node Empty 3 Empty) 4 (Node Empty 5 Empty))
> (untz . updateTZ 42 . left . right . mktz) tr
Node (Node Empty 1 Empty) 2 (Node (Node Empty 42 Empty) 4 (Node Empty 5 Empty))
-}

{-
-- 1
data Unit = Unit
-- 1 + 1 (== 2)
data Bool = True | False -- или Either Unit Unit
-- 1 + 1 + 1 (== 3)
data Triple = T0 | T1 | T2
-- 1 + 1 + 1 + 1 (==4)
data Quad = Zero | One | Two | Three
-- 2 * 2 (==4)
type Quad' = (Bool, Bool) -- или Quad' Bool Bool
-- 2 ^ 3
type Octo = Triple -> Bool
- 3 ^ 2
type Nano = Bool -> Triple

Рекурсивные типы : Алгебра списков
L(x) = 1 + x + x^2 + x^3 + ...
x * L(x) =  x + x^2 + x^3 + ...
1 + x * L(x) = 1 + x + x^2 + x^3 + ...
1 + x * L(x) = L(x)
L(x) = 1 + x * L(x)
Ага, как в Хаскеле
data List x = Nil | Cons x (List x)

Рекурсивные типы :  Более регулярный подход
data Nat = Zro | Suc Nat
nat = 1 + nat

Аналогия с лямбда-исчислением
Пусть F определен рекурсивно
F = G[F]
F = (λx. G[F:=x]) F
F = Y (λx. G[x])

В теории типов для этого используют μ-нотацию
nat = μx. 1 + x

list a = 1 + a * list a

list a = μx. 1 + a * x


Напомним, что мы определяли список так
L(x) = 1 + x + x^2 + x^3 + ...
Или рекурсивно
L(x) = 1 + x * L(x)
Можно использовать еще более экстремистский подход
L(x) - x * L(x) = 1
L(x) * (1 - x) = 1
L(x) = 1 / (1 - x)
Сравним первое и последнее, вспомнив ряды.

Дифференцируем

L'(x) = 1 * L(x) + x * L'(x)
L'(x) (1 - x) = L(x)
L'(x) = L(x) / (1 - x)
L'(x) = L(x) * (1 + x + x^2 + x^3 + ...)
L'(x) = L(x) * L(x) -- Ой, мама, зиппер!
(Точнее контекст с одним фокусом для списка)


Т(x) = 1 + x * T^2(x)
Т'(x) = T^2(x) + x * 2 * T(x) * T'(x)
Т'(x) = T(x)(T(x) + x * 2 * T'(x))
Т'(x) = T^2(x) / (1 - 2 * x * T(x))
Т'(x) = T^2(x) * L(2 * x * T(x))

На языке контекстов с одним фокусом:

T^2(x) - два поддерева ниже фокуса
L(2 * x * T(x)) = [(Bool,x,Tree x)]
 Bool   - указывает, идти вправо или влево
 x      - значение родительского узла
 Tree x - второе поддерево родительского узла
-}