﻿module Fp10 where

import Control.Monad.Reader
import Control.Monad.State
import Control.Monad.Writer
import Data.IORef
import Data.List
import Data.Maybe (fromMaybe)
import System.IO
import System.Random
import Data.Function ( on )


-----------------------------------------------------
-- Монада IO

main :: IO ()
main = do
  putStrLn "What is your name?"
  name <- getLine'
  putStrLn ("Nice to meet you, " ++ name ++ "!")

main' :: IO ()
main' =
  putStrLn "What is your name?"
    >> getLine' >>= \name ->
      putStrLn $ "Nice to meet you, " ++ name ++ "!"

getLine' :: IO String
getLine' = do
  c <- getChar
  if c == '\n'
    then return []
    else do
      cs <- getLine'
      return (c : cs)

putStr' :: String -> IO ()
putStr' [] = return ()
putStr' (x : xs) = putChar x >> putStr' xs

putStr'' :: String -> IO ()
putStr'' = sequence_ . map putChar

putStr''' :: String -> IO ()
putStr''' = mapM_ putChar

-----------------------------------------------------
-- Монада Reader

-- простейший Reader (instance Monad ((->) e) where ...)
doIt :: Int -> Int
doIt = do
  a <- (^ 2)
  b <- (* 5)
  return $ a + b

-- >>> doIt 3
-- 24

-- тип окружения - r
simpleReader :: Show r => Reader r String
simpleReader = reader (\e -> "Environment is " ++ show e)

-- >>> runReader simpleReader 42
-- "Environment is 42"

-- >>> runReader ask "Hello!"
-- "Hello!"

type User = String

type Password = String

type UsersTable = [(User, Password)]

pwds :: UsersTable
pwds = [("Bill", "123"), ("Ann", "qwerty"), ("John", "2sRq8P")]

-- возвращает имя первого пользователя в списке
firstUser :: Reader UsersTable User
firstUser = do
  e <- ask
  let name = fst (head e)
  return name

-- >>> runReader firstUser pwds
-- "Bill"

-- возвращает длину пароля пользователя  или -1, если такого пользователя нет
getPwdLen :: User -> Reader UsersTable Int
getPwdLen person = do
  mbPwd <- asks $ lookup person
  let mbLen = fmap length mbPwd
  let len = fromMaybe (-1) mbLen
  return len

-- >>> runReader (getPwdLen "Ann") pwds
-- 6

-- >>> runReader (getPwdLen "Ann") []
-- -1

usersCount :: Reader UsersTable Int
usersCount = asks length

localTest :: Reader UsersTable (Int, Int)
localTest = do
  count1 <- usersCount
  count2 <- local (("Mike", "1") :) usersCount
  return (count1, count2)

-- >>> runReader localTest pwds
-- (3,4)

-----------------------------------------------------
-- Монада Writer

-- >>> runWriter (return 3 :: Writer String Int)
-- (3,"")

type Vegetable = String

type Price = Double

type Qty = Double

type Cost = Double

type PriceList = [(Vegetable, Price)]

prices :: PriceList
prices = [("Potato", 13), ("Tomato", 55), ("Apple", 48)]

-- tell позволяет задать вывод
addVegetable :: Vegetable -> Qty -> Writer (Sum Cost) (Vegetable, Price)
addVegetable veg qty = do
  let pr = fromMaybe 0 $ lookup veg prices
  let cost = qty * pr
  tell $ Sum cost
  return (veg, pr)

-- >>> runWriter $ addVegetable "Apple" 100
-- (("Apple",48.0),Sum {getSum = 4800.0})

-- суммарная стоимость копится <<за кадром>>
myCart0 :: Writer (Sum Cost) [(Vegetable, Price)]
myCart0 = do
  x1 <- addVegetable "Potato" 3.5
  x2 <- addVegetable "Tomato" 1.0
  x3 <- addVegetable "AGRH!!" 1.6
  return [x1, x2, x3]

-- >>> runWriter myCart0
-- ([("Potato",13.0),("Tomato",55.0),("AGRH!!",0.0)],Sum {getSum = 100.5})

-- если хотим знать промежуточные стоимости, используем listen ...
myCart1 :: Writer (Sum Cost) [((Vegetable, Price), Sum Cost)]
myCart1 = do
  x1 <- listen $ addVegetable "Potato" 3.5
  x2 <- listen $ addVegetable "Tomato" 1.0
  x3 <- listen $ addVegetable "AGRH!!" 1.6
  return [x1, x2, x3]

-- >>> runWriter myCart1
-- ([(("Potato",13.0),Sum {getSum = 45.5}),(("Tomato",55.0),Sum {getSum = 55.0}),(("AGRH!!",0.0),Sum {getSum = 0.0})],Sum {getSum = 100.5})

-- ... или listens
myCart1' :: Writer (Sum Cost) [((Vegetable, Price), Cost)]
myCart1' = do
  x1 <- listens getSum $ addVegetable "Potato" 3.5
  x2 <- listens getSum $ addVegetable "Tomato" 1.0
  x3 <- listens getSum $ addVegetable "AGRH!!" 1.6
  return [x1, x2, x3]

-- >>> runWriter myCart1'
-- ([(("Potato",13.0),45.5),(("Tomato",55.0),55.0),(("AGRH!!",0.0),0.0)],Sum {getSum = 100.5})

--  (pass технический хелпер для censor)
--  для модификации лога используют censor :: (w -> w) -> Writer a -> Writer a
myCart0' :: Writer (Sum Cost) [(Vegetable, Price)]
myCart0' = censor (discount 10) myCart0

-- бизнес-логика:)
discount :: (Ord a, Fractional a) => a -> Sum a -> Sum a
discount proc (Sum x) =
  Sum $
    if x < 100
      then x
      else x * (100 - proc) / 100

-- >>> runWriter myCart0'
-- ([("Potato",13.0),("Tomato",55.0),("AGRH!!",0.0)],Sum {getSum = 90.45})

-----------------------------------------------------
-- Монада State

-- >>> runState (return 3 :: State String Int) "Hi from State!"
-- (3,"Hi from State!")

tick :: State Int Int
tick = do
  n <- get
  put (n + 1)
  return n

succ' :: Int -> Int
succ' n = execState tick n

plus :: Int -> Int -> Int
plus n x = execState (sequence $ replicate n tick) x

plus' :: Int -> Int -> Int
plus' n x = execState (replicateM n tick) x

data Logged a = Logged String a
  deriving (Eq, Show)

instance Functor Logged where
  fmap f (Logged log x) = Logged log (f x)

instance Applicative Logged where
  pure x = Logged "" x
  (Logged flog f) <*> (Logged xlog x) = Logged (xlog ++ flog) (f x)

instance Monad Logged where
  (Logged alog a) >>= k = let (Logged blog b) = k a in Logged (blog ++ alog) b

write2log :: String -> Logged ()
write2log s = Logged s ()

logIt :: Show b => b -> Logged b
logIt v = do
  write2log $ "var = " ++ show v ++ "; "
  return v

test :: Logged Integer
test = do
  x <- logIt 3
  y <- logIt 5
  let res = x + y
  write2log $ "sum = " ++ show res ++ "; "
  return res

-- >>> (+) <$> (Logged "a" 2) <*> (Logged "b" 3)
-- Logged "ba" 5

-- >>> test
-- Logged "sum = 8; var = 5; var = 3; " 8

fac :: Int -> Integer
fac n = fst $ execState (replicateM n facStep) (1, 0)

facStep :: State (Integer, Integer) ()
facStep = do
  (f, i) <- get
  let next = i + 1
  put (f * next, next)

fac' :: Integer -> Integer
fac' n = execState (forM [1 .. n] facStep') 1

facStep' :: Integer -> State Integer ()
facStep' i = modify' (* i)

-- >>> fac 5
-- >>> fac' 5
-- 120
-- 120

testIORef :: IO [Integer]
testIORef = do
  x <- newIORef 1
  val1 <- readIORef x
  writeIORef x 41
  val2 <- readIORef x
  modifyIORef' x succ
  val3 <- readIORef x
  return [val1, val2, val3]

-- >>> testIORef
-- [1,41,42]

fac'' :: Integer -> IO Integer
fac'' n = do
  x <- newIORef 1
  mapM_ (\i -> modifyIORef' x (* i)) [1 .. n]
  readIORef x

-- >>> fac'' 5
-- 120

randomRState :: (Random a, RandomGen g) => (a, a) -> State g a
randomRState (x, y) = do
  gen <- get
  let (r, gen') = randomR (x, y) gen
  put gen'
  return r

randomRState' :: (Random a, RandomGen g) => (a, a) -> State g a
randomRState' (x, y) = state (randomR (x, y))

doWork :: State StdGen ([Int], [Int])
doWork = do
  xs <- replicateM 5 $ randomRState' (1, 6)
  ys <- replicateM 5 $ randomRState' (1, 6)
  return (xs, ys)

-- >>> evalState doWork (mkStdGen 42)
-- ([6,4,2,5,3],[2,1,6,1,4])

main''' :: IO ()
main''' = do
  let txt = "Some text"
  handle <- openFile "Text.txt" WriteMode
  hPutStrLn handle txt
  hClose handle

  putStrLn "Hit any key to continue..."
  ignore <- getChar

  withFile "Text.txt" ReadMode $ hGetContents >=> putStrLn

  putStrLn "Hit any key to continue..."
  ignore <- getChar
  return ()

minusLoggedL :: (Show a, Num a) => a -> [a] -> Writer String a
minusLoggedL x xs = censor appendBraces minusFold
  where
    minusFold = do
      tell $ show x
      foldM recComp x xs

    recComp :: (Show a, Num a) => a -> a -> Writer String a
    recComp acc a = do
      tell $ "-" ++ show a ++ ")"
      return $ acc - a

    appendBraces w =
      let n = length $ elemIndices ')' w
       in replicate n '(' ++ w

-- >>> runWriter $ minusLoggedL 0 [1..3]
-- (-6,"(((0-1)-2)-3)")

minusLoggedR :: (Show a, Num a) => a -> [a] -> Writer String a
minusLoggedR x xs = censor appendBraces minusFold
  where
    minusFold = do
      tell $ show x
      foldM recComp x (reverse xs)

    recComp :: (Show a, Num a) => a -> a -> Writer String a
    recComp acc a = do
      tell $ "-" ++ show a ++ "("
      return $ a - acc

    appendBraces w =
      let n = length $ elemIndices '(' w
       in reverse $ replicate n ')' ++ w

-- >>> runWriter $ minusLoggedR 0 [1..3]
-- (2,"(1-(2-(3-0)))")

fib :: Int -> Integer
fib n = fst $ execState (replicateM n fibStep) (0, 1)

fibStep :: State (Integer, Integer) ()
fibStep = do
  (cur, prev) <- get
  put (cur + prev, cur)

-- >>> map fib [0 .. 8]
-- [0,1,1,2,3,5,8,13,21]

while :: IORef a -> (a -> Bool) -> IO () -> IO ()
while ref p action = do
  ival <- readIORef ref
  when (p ival) $ action >> while ref p action

factorial :: Integer -> IO Integer
factorial n = do
  r <- newIORef 1
  i <- newIORef 1
  while i (<= n) ( do
      ival <- readIORef i
      modifyIORef' r (* ival)
      modifyIORef' i (+ 1)
    )
  readIORef r

-- >>> factorial 5
-- 120

avgdev :: Int -> Int -> State StdGen Double
avgdev k n = average <$> replicateM k (runSeries n)
  where
    average xs = realToFrac (sum xs) / genericLength xs
    runSeries :: Int -> State StdGen Int
    runSeries n = do
      xs <- replicateM n $ randomRState' (0, 1) :: State StdGen [Int]
      let m = n `div` 2
      let heads = length $ filter (==1) xs
      return $ abs (m - heads)

-- >>> evalState (avgdev 100 100) <$> newStdGen
-- 4.05

avgdev' :: Int -> Int -> Double
avgdev' k n = average . snd $ foldl runSeries (rs, []) [1 .. k]
  where
    rs = randomRs (0, 1) (mkStdGen 42) :: [Int]
    average xs = realToFrac (sum xs) / genericLength xs
    runSeries (rs, acc) _ = (tail, d : acc)
      where
        (ys, tail) = splitAt n rs
        m = n `div` 2
        heads = length $ filter (==1) ys
        d = abs (m - heads)

-- >>> evalState (avgdev 100 100) (mkStdGen 42)
-- >>> avgdev' 100 100
-- 4.14
-- 4.14


devs :: Int -> Int -> State StdGen [Int]
devs k n = replicateM k (runSeries n)
  where
    runSeries n = do
      xs <- replicateM n $ randomRState' (0, 1) :: State StdGen [Int]
      let m = n `div` 2
      let heads = length $ filter (==1) xs
      return $ m - heads

hist :: [Int] -> [String]
hist = map toRow . group . sort
  where
    toRow l = show (head l) ++ "\t" ++ replicate (length l) 'x'

divsHist :: Int -> Int -> IO ()
divsHist k n = do
  gen <- getStdGen
  let ds = evalState (devs k n) gen
  mapM_ putStrLn $ hist ds

-- >>> evalState (devs 10 10) (mkStdGen 42)
-- [-1,-1,4,2,3,2,0,0,2,4]

-- >>> hist [-1,-1,4,2,3,2,0,0,2,4]
-- ["-1\txx","0\txx","2\txxx","3\tx","4\txx"]
