module Main where

import Data.List (unwords)

example :: [String]
example = ["This", "is", "an", "example", "of", "evaluation"]

main :: IO ()
main = putStrLn $ unwords example

