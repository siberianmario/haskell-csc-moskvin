{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE UndecidableInstances #-}

module Fp11 where

import Control.Applicative
import Control.Monad.Cont
import Control.Monad.Except
import Control.Monad.Fail
import Control.Monad.Identity
import Control.Monad.Reader
import Control.Monad.State
import Control.Monad.Trans.Maybe
import Control.Monad.Writer
import Data.Char
import Data.List
import Data.Maybe
import Data.Monoid
import Text.Parsec (Parsec, parse, string)
import Text.Regex.Applicative (match, psym, sym)

-- >>> Nothing <|> (Just 3) <|> (Just 5) <|> Nothing
-- Just 3

parser :: Parsec String () String
parser = string "ABC" <|> string "DEF"

-- >>> parse parser "" "ABC123"
-- Right "ABC"

-- >>> parse parser "" "DEF123"
-- Right "DEF"

-- >>> parse parser "" "GHI123"
-- Left (line 1, column 1):
-- unexpected "G"
-- expecting "ABC" or "DEF"

-- >>> match (sym 'a' <|> sym 'b') "a"
-- Just 'a'
-- >>> match (many $ psym isAlpha) "abc"
-- Just "abc"
-- >>> match (many $ psym isAlpha) "123"
-- Nothing
-- >>> match (many $ psym isAlpha) ""
-- Just ""
-- >>> match (some $ psym isAlpha) ""
-- Nothing

-- >>> Nothing `mplus` (Just 3) `mplus` (Just 5) `mplus` Nothing
-- Just 3

pythags :: [(Integer, Integer, Integer)]
pythags = do
  z <- [1 ..]
  x <- [1 .. z]
  y <- [x .. z]
  guard $ x ^ 2 + y ^ 2 == z ^ 2
  return (x, y, z)

-- >>> take 5 pythags
-- [(3,4,5),(6,8,10),(5,12,13),(9,12,15),(8,15,17)]

--------------------------------------------------

data Vector = Vector Int Int
  deriving (Eq, Show)

data Matrix = Matrix Vector Vector
  deriving (Eq, Show)

class Mult a b c | a b -> c where
  (***) :: a -> b -> c

instance Mult Matrix Matrix Matrix where
  Matrix (Vector a11 a12) (Vector a21 a22) *** Matrix (Vector b11 b12) (Vector b21 b22) =
    Matrix (Vector c11 c12) (Vector c21 c22)
    where
      c11 = a11 * b11 + a12 * b21
      c12 = a11 * b12 + a12 * b22
      c21 = a21 * b11 + a22 * b21
      c22 = a21 * b12 + a22 * b22

a :: Matrix
a = Matrix (Vector 1 2) (Vector 3 4)

i :: Matrix
i = Matrix (Vector 1 0) (Vector 0 1)

-- >>> a *** i
-- Matrix (Vector 1 2) (Vector 3 4)

--------------------------------------------------
-- MonadError
data DivByError = ErrZero | Other String
  deriving (Eq, Read, Show)

-- instance Error DivByError where
--   strMsg s = Other s
--   noMsg = Other "Unknown DivByError"

instance Semigroup DivByError where
  ErrZero <> e = e
  e <> _ = e

instance Monoid DivByError where
  mempty = Other "Unknown DivByError"

(/?) :: Double -> Double -> Either DivByError Double
x /? 0 = throwError ErrZero
x /? y = return $ x / y

example0 :: Double -> Double -> Either DivByError String
example0 x y = action `catchError` handler
  where
    action = do
      q <- x /? y
      return $ show q
    handler = return . show

-- >>> example0 5 2
-- >>> example0 5 0
-- Right "2.5"
-- Right "ErrZero"

-- требуется расширение FlexibleContexts
(?/?) ::
  (MonadError DivByError m) =>
  Double ->
  Double ->
  m Double
x ?/? 0 = throwError ErrZero
x ?/? y = return $ x / y

example1 :: Double -> Double -> Either DivByError String
example1 x y = action `catchError` handler
  where
    action = do
      q <- x ?/? y
      return $ show q
    handler = return . show

-- >>> example1 5 2
-- >>> example1 5 0
-- Right "2.5"
-- Right "ErrZero"

example2 :: Double -> Double -> Either DivByError String
example2 x y = runIdentity $ runExceptT $ action `catchError` handler
  where
    action = do
      q <- x ?/? y
      guard $ y >= 0
      return $ show q
    handler = return . show

-- >>> example2 7 (-5)
-- Right "Other \"Unknown DivByError\""

--------------------------------------------------------

stInteger :: State Integer Integer
stInteger = modify (+ 1) >> get

stString :: State String String
stString = modify (++ "1") >> get

-- >>> evalState stInteger 0
-- >>> evalState stString "0"
-- 1
-- "01"

stComb :: StateT Integer (StateT String Identity) (Integer, String)
stComb = do
  modify (+ 1)
  lift $ modify (++ "1")
  a <- get
  b <- lift get
  return (a, b)

-- >>> runIdentity $ evalStateT (evalStateT stComb 0) "0"
-- (1,"01")

newtype MyMaybeT m a = MyMaybeT {runMyMaybeT :: m (Maybe a)}

instance (Functor m) => Functor (MyMaybeT m) where
  fmap f = MyMaybeT . fmap (fmap f) . runMyMaybeT

instance (Applicative m) => Applicative (MyMaybeT m) where
  pure a = MyMaybeT $ pure (Just a)
  MyMaybeT mf <*> MyMaybeT mx = MyMaybeT $ (<*>) <$> mf <*> mx

instance (Monad m) => Monad (MyMaybeT m) where
  fail _ = MyMaybeT $ return Nothing
  return = lift . return
  x >>= f = MyMaybeT $ do
    v <- runMyMaybeT x
    case v of
      Nothing -> return Nothing
      Just y -> runMyMaybeT (f y)

instance MonadTrans MyMaybeT where
  lift = MyMaybeT . fmap Just

instance (Monad m) => MonadFail (MyMaybeT m) where
  fail _ = MyMaybeT $ return Nothing

mbSt :: MyMaybeT (StateT Integer Identity) Integer
mbSt = do
  lift $ modify (+ 1)
  a <- lift get
  True <- return $ a >= 3 -- guard (a >= 3)
  return a

-- >>> runIdentity $ evalStateT (runMyMaybeT mbSt) 0
-- >>> runIdentity $ evalStateT (runMyMaybeT mbSt) 2
-- Nothing
-- Just 3

-- instance (Applicative m) =>  Alternative (MyMaybeT m) where
--   empty = MyMaybeT $ pure Nothing
--   MyMaybeT mx <|> MyMaybeT my = MyMaybeT $ (<|>) <$> mx <*> my

-- чтобы заработал guard нужно сделать MyMaybeT m представителем Alternative
instance (Monad m) => Alternative (MyMaybeT m) where
  empty = MyMaybeT $ return Nothing
  x <|> y = MyMaybeT $ do
    v <- runMyMaybeT x
    case v of
      Nothing -> runMyMaybeT y
      Just _ -> return v

mbSt' :: MyMaybeT (State Integer) Integer
mbSt' = do
  lift $ modify (+ 1)
  a <- lift get
  guard $ a >= 3 -- !!
  return a

-- >>> runIdentity $ evalStateT (runMyMaybeT mbSt') 0
-- >>> runIdentity $ evalStateT (runMyMaybeT mbSt') 2
-- Nothing
-- Just 3

-- Для любой пары монад можно избавиться от подъёма
-- стандартных операций вложенной монады

class Monad m => MyMonadState s m | m -> s where
  get' :: m s
  get' = state' (\s -> (s, s))

  put' :: s -> m ()
  put' s = state' (const ((), s))

  state' :: (s -> (a, s)) -> m a
  state' f = do
    s <- get'
    let (a, s') = f s
    put' s'
    return a

modifys' :: MyMonadState s m => (s -> s) -> m ()
modifys' f = state' (\s -> ((), f s))

gets' :: MyMonadState s m => (s -> a) -> m a
gets' f = f <$> get'

instance (MyMonadState s m) => MyMonadState s (MyMaybeT m) where
  get' = lift get'
  put' = lift . put'

instance MyMonadState s (State s) where
  get' = get
  put' = put
  state' = state

mbSt'' :: MyMaybeT (State Integer) Integer
mbSt'' = do
  modifys' (+ 1) -- без lift
  a <- get' -- без lift
  guard $ a >= 3
  return a

-- >>> runIdentity $ evalStateT (runMyMaybeT mbSt'') 0
-- >>> runIdentity $ evalStateT (runMyMaybeT mbSt'') 2
-- Nothing
-- Just 3

data ListIndexError
  = ErrTooLargeIndex Int
  | ErrNegativeIndex
  | OtherErr String
  deriving (Eq, Show)

infixl 9 !!!

(!!!) :: (MonadError ListIndexError m) => [a] -> Int -> m a
xs !!! n
  | n < 0 = throwError ErrNegativeIndex
  | n >= length xs = throwError $ ErrTooLargeIndex n
  | otherwise = return $ xs !! n

-- >>> [1,2,3] !!! 0      :: Either ListIndexError Int
-- >>> [1,2,3] !!! (-10)  :: Either ListIndexError Int
-- >>> [1,2,3] !!! 3      :: Either ListIndexError Int
-- Right 1
-- Left ErrNegativeIndex
-- Left (ErrTooLargeIndex 3)

------------------------------------------------------

data Excep a = Err String | Ok a
  deriving (Eq, Show)

instance Functor Excep where
  fmap f (Err msg) = Err msg
  fmap f (Ok x) = Ok $ f x

instance Applicative Excep where
  pure x = Ok x
  Err msg <*> _ = Err msg
  _ <*> Err msg = Err msg
  Ok f <*> Ok x = Ok $ f x

instance Monad Excep where
  Err msg >>= _ = Err msg
  Ok a >>= k = k a

instance Alternative Excep where
  empty = Err "Alternative.empty error."
  Err x <|> Err y = Err $ x ++ y
  Err _ <|> Ok a = Ok a
  Ok a <|> _ = Ok a

instance MonadError String Excep where
  throwError msg = Err msg
  catchError (Ok a) _ = Ok a
  catchError (Err e) f = f e

instance MonadFail Excep where
  fail _ = Err "Monad.fail error."

(?/) :: (MonadError String m) => Double -> Double -> m Double
x ?/ 0 = throwError "Division by 0."
x ?/ y = return $ x / y

example :: Double -> Double -> Excep String
example x y = action `catchError` return
  where
    action = do
      q <- x ?/ y
      guard (q >= 0)
      if q > 100
        then do
          100 <- return q
          undefined
        else return $ show q

-- >>> example 5 2
-- >>> example 5 0
-- >>> example 5 (-2)
-- >>> example 5 0.002
-- Ok "2.5"
-- Ok "Division by 0."
-- Ok "Alternative.empty error."
-- Ok "Monad.fail error."

-----------------------------------------------------------

data ParseError = ParseError {location :: Int, reason :: String}

type ParseMonad = Either ParseError

parseHex :: String -> ParseMonad Integer
parseHex str = foldl hexFolding 0 <$> traverse parseChar (zip [1 ..] str)
  where
    parseChar (i, c) =
      if isHexDigit c
        then return $ digitToInt c
        else throwError $ ParseError i (c : ": invalid digit")
    hexFolding acc n = acc * 16 + toInteger n

printError :: ParseError -> ParseMonad String
printError (ParseError location reason) =
  return $ "At pos " ++ show location ++ ": " ++ reason

test :: [Char] -> String
test s = str
  where
    (Right str) =
      do
        n <- parseHex s
        return $ show n
        `catchError` printError

-- >>> test "DEADBEEF"
-- >>> test "DEADMEAT"
-- "3735928559"
-- "At pos 5: M: invalid digit"

-----------------------------------------------------------

newtype PwdError = PwdError String

instance Semigroup PwdError where
  PwdError x <> PwdError y = PwdError $ x ++ y

instance Monoid PwdError where
  mempty = PwdError "password error"

type PwdErrorMonad = ExceptT PwdError IO

askPassword :: PwdErrorMonad ()
askPassword = do
  liftIO $ putStrLn "Enter your new password:"
  value <- msum $ repeat getValidPassword
  liftIO $ putStrLn "Storing in database..."

getValidPassword :: PwdErrorMonad String
getValidPassword = do
  s <- liftIO getLine
  liftEither (validate s) `catchError` logError
  return s

logError :: PwdError -> PwdErrorMonad ()
logError (PwdError e) = do
  liftIO $ putStrLn ("Incorrect input: " ++ e)
  throwError $ PwdError e

validate :: String -> Either PwdError ()
validate s = do
  ensure (length s >= 8) $ PwdError "password is too short!"
  ensure (any isNumber s) $ PwdError "password must contain some digits!"
  ensure (any isPunctuation s) $ PwdError "password must contain some punctuations!"

ensure :: MonadError e m => Bool -> e -> m ()
ensure b e = if b then return () else throwError e