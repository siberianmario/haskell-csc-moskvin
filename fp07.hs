module Fp07 where

import Data.List (inits, tails, unfoldr)
import Data.Foldable (fold)
import Data.Sequence (Seq((:<|), Empty), singleton, (|>))

newtype Endo a = Endo {appEndo :: a -> a}

instance Semigroup (Endo a) where
  Endo f <> Endo g = Endo (f . g)

instance Monoid (Endo a) where
  mempty = Endo id

fn :: Endo Integer
fn = mconcat $ map Endo [(+ 5), (* 3), (^ 2)]

-- >>> appEndo fn 2
-- 17

myFoldr :: Foldable t => (a -> b -> b) -> b -> t a -> b
myFoldr f z t = appEndo (foldMap (Endo . f) t) z

newtype Dual a = Dual {getDual :: Endo a}

instance Semigroup (Dual a) where
  Dual (Endo f) <> Dual (Endo g) = Dual (Endo (g . f))

instance Monoid (Dual a) where
  mempty = Dual (Endo id)

myFoldl :: Foldable t => (a -> b -> a) -> a -> t b -> a
myFoldl f z t = appEndo (getDual (foldMap (Dual . Endo . flip f) t)) z

instance Semigroup Int where
  (<>) = (+)

-- >>> mconcat ([Just 5, Nothing, Just 3] :: [Maybe Int])
-- Just 8

-- >>> foldl (/) 480 [3,2,5,2]
-- 8.0

-- >>> myFoldl (/) 480 [3,2,5,2]
-- 8.0

concatWith :: (Foldable t, Semigroup a) => a -> t a -> a
concatWith s = foldr1 (\x y -> x <> s <> y)

-- >>> concatWith "," ["ab","cde","fgh"]
-- "ab,cde,fgh"

or' :: [Bool] -> Bool
or' = foldr (||) False

-- >>> or' [False, False, False]
-- False

length' :: [a] -> Int
length' = foldr (\_ l -> l + 1) 0

-- >>> length' [False, False, False]
-- 3

maximum' :: Ord a => [a] -> a
maximum' = foldr1 max

-- >>> maximum' [1, 5, -4]
-- 5

head' :: [a] -> a
head' = foldl1 const

-- >>> head' [1, 5, -4]
-- 1

last' :: [a] -> a
last' = foldr1 (\_ l -> l)

-- >>> last' [1, 5, -4]
-- -4

filter' :: (a -> Bool) -> [a] -> [a]
filter' p = foldr (\a tail -> if p a then a : tail else tail) []

-- >>> filter' (> 0) [1, 5, -4]
-- [1,5]

map' :: (a -> b) -> [a] -> [b]
map' f = foldr (\a tail -> (f a) : tail) []

-- >>> map' (*2) [1, 5, -4]
-- [2,10,-8]

take' :: Int -> [a] -> [a]
take' n xs = foldr step ini xs n
  where
    ini :: Int -> [a]
    ini = const []
    step :: a -> (Int -> [a]) -> (Int -> [a])
    step x g = f
      where
        f n
          | n <= 0 = []
          | otherwise = x : g (n - 1)

-- >>> take' 2 "abc"
-- "ab"

revRange :: (Char, Char) -> [Char]
revRange (a, b) = unfoldr (fun a) b
  where
    fun :: Char -> Char -> Maybe (Char, Char)
    fun bot x
      | x >= bot = Just (x, pred x)
      | otherwise = Nothing

-- >>> revRange ('a', 'x')
-- "xwvutsrqponmlkjihgfedcba"

tails' :: [a] -> [[a]]
tails' = foldr fun [[]]
  where
    fun a t@(x : xs) = (a : x) : t

inits' :: [a] -> [[a]]
inits' = foldr fun [[]]
  where
    fun :: a -> [[a]] -> [[a]]
    fun a t = [] : map (a :) t

-- >>> inits' "abc"
-- ["","a","ab","abc"]

reverse' :: [a] -> [a]
reverse' = foldr (\a xs -> xs ++ [a]) []

reverse'' :: [a] -> [a]
reverse'' = foldl (flip (:)) []

-- >>> reverse'' "abcd"
-- "dcba"

(!!!) :: [a] -> Int -> Maybe a
xs !!! n = foldr step ini xs n
  where
    ini :: Int -> Maybe a
    ini = const Nothing
    step :: a -> (Int -> Maybe a) -> (Int -> Maybe a)
    step x f 0 = Just x
    step x f n = f (n - 1)

-- >>> [1..10] !!! 1
-- Just 2

foldl'' :: (b -> a -> b) -> b -> [a] -> b
foldl'' f v xs = foldr (fun f) ini xs v
  where
    ini :: b -> b
    ini = id
    fun :: (b -> a -> b) -> a -> (b -> b) -> b -> b
    fun f a g = g . flip f a
-- >>> foldl'' (\acc x -> acc ++ [x]) [] "123"
-- "123"


data Tree a = Nil | Branch (Tree a) a (Tree a)
  deriving (Eq, Show)

newtype Preorder a = PreO (Tree a) deriving (Eq, Show)
newtype Postorder a = PostO (Tree a) deriving (Eq, Show)
newtype Levelorder a = LevelO (Tree a) deriving (Eq, Show)

leaf :: a -> Tree a
leaf a = Branch Nil a Nil

lt :: Tree String
lt = Branch (leaf "A") "B" (Branch (leaf "C") "D" (leaf "E"))

tr :: Tree String
tr = Branch Nil "G" (Branch (leaf "H") "I" Nil)

tree :: Tree String
tree = Branch lt "F" tr

instance Foldable Tree where
  foldr f z Nil = z
  foldr f z (Branch lt a rt) = foldr f (f a (foldr f z rt)) lt

instance Foldable Preorder where
  foldr f z (PreO Nil) = z
  foldr f z (PreO (Branch lt a rt)) = f a (foldr f (foldr f z (PreO rt)) (PreO lt))

instance Foldable Postorder where
  foldr f z (PostO Nil) = z
  foldr f z (PostO (Branch lt a rt)) = foldr f (foldr f (f a z) (PostO rt)) (PostO lt)

instance Foldable Levelorder where
  foldMap f t = helper mempty (singleton t)
    where
      helper m Empty = m
      helper m (t :<| q) = helper2 t
        where
          helper2 (LevelO Nil) = helper m q
          helper2 (LevelO (Branch lt a rt)) = helper (m <> f a) (q |> LevelO lt |> LevelO rt) 

-- >>> fold tree
-- >>> fold (PreO tree)
-- >>> fold (PostO tree)
-- >>> fold (LevelO tree)
-- "ABCDEFGHI"
-- "FBADCEGIH"
-- "ACEDBHIGF"
-- "FBGADICEH"
