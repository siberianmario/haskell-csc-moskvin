{-# LANGUAGE TemplateHaskell #-}
module Fp14lens where
import Control.Lens

{-
Линза - инструмент для манипулирования подструктурой некоторой структуры данных.
Технически линза - это АТД составленный из пары геттер-сеттер
lens :: (c -> a) -> (c -> a -> c) -> Lens' c a
-}

data Arc = Arc { 
  _degree, _minute, _second :: Int 
  } deriving Show
  
data Location = Location { 
  _latitude, _longitude :: Arc 
  } deriving Show
-- символ подчеркивания в полях записи является конвенцией, принятой в Control.Lens для генерации линз с помощью TH.

-- TH создает нужные функции ...
$(makeLenses ''Location)
-- ... а именно
-- latitude  :: Lens' Location Arc
-- longitude :: Lens' Location Arc
-- на самом деле типы такие
-- latitude :: Functor f => (Arc -> f Arc) -> Location -> f Location

$(makeLenses ''Arc)
