module Fp05 where

import Data.Sequence (mapWithIndex)
import Data.List (elemIndices, (\\))

-- переставляет элементы пары местами (определена в Data.Tuple)
swap :: (a, b) -> (b, a)
swap (x, y) = (y, x)

-- перечисление
data Color = Red | Green | Blue | Indigo | Violet deriving (Show)

isRGB :: Color -> Bool
isRGB Red = True
isRGB Green = True
isRGB Blue = True
isRGB _ = False -- Wild-card

-- декартово произведение
data PointDouble = PtD Double Double deriving (Show)

midPointDouble :: PointDouble -> PointDouble -> PointDouble
midPointDouble (PtD x1 y1) (PtD x2 y2) = PtD ((x1 + x2) / 2) ((y1 + y2) / 2)

-- полиморфные типы
data Point a = Pt a a deriving (Show)

midPoint :: Fractional a => Point a -> Point a -> Point a
midPoint (Pt x1 y1) (Pt x2 y2) = Pt ((x1 + x2) / 2) ((y1 + y2) / 2)

-- рекурсивные типы
data List a = Nil | Cons a (List a) deriving (Show)

len :: List a -> Int
len (Cons _ xs) = 1 + len xs
len Nil = 0

-- выражение case ... of ...
head' xs = case xs of
  (x : _) -> x
  [] -> error "head': empty list"

-- образцы в лямбде
head'' = \(x : _) -> x

-- Семантика сопоставления с образцом
foo (1, 2) = 3
foo (0, _) = 5

-- As-образец
dupFirst :: [a] -> [a]
dupFirst (x : xs) = x : x : xs

dupFirst' :: [a] -> [a]
dupFirst' s@(x : xs) = x : s

-- ленивые образцы
(***) f g ~(x, y) = (f x, g y)

-- Метки полей (Field Labels)
data Point' a = Pt' {ptx, pty :: a} deriving (Show)

absP p = sqrt (ptx p ^ 2 + pty p ^ 2)

absP' (Pt' {ptx = x, pty = y}) = sqrt (x ^ 2 + y ^ 2)

-- Стандартные алгебраические типы
head''' :: [a] -> Either String a
head''' (x : _) = Right x
head''' [] = Left "head'': empty list"

swap2 :: [a] -> [a]
swap2 [] = []
swap2 [x] = [x]
swap2 (x : y : tail) = y : x : swap2 tail

zip2 :: [a] -> [b] -> [(a, b)]
zip2 [] _ = []
zip2 _ [] = []
zip2 (x : xs) (y : ys) = (x, y) : zip2 xs ys

count2 :: Num a => [a] -> [a] -> [a]
count2 xs ys = map (uncurry (+)) (zip2 xs ys)

myReverse :: [a] -> [a]
myReverse a = helper a []
  where
    helper [] tail = tail
    helper (x : xs) tail = helper xs (x : tail)

mapIf :: (a -> Bool) -> (a -> a) -> [a] -> [a]
mapIf p f [] = []
mapIf p f (x : xs)
  | p x = f x : mapIf p f xs
  | otherwise = x : mapIf p f xs

doubleEven :: Integral a => [a] -> [a]
doubleEven = mapIf even (* 2)

-- >>> doubleEven [0..10]
-- [0,1,4,3,8,5,12,7,16,9,20]

zeroOdd :: Integral a => [a] -> [a]
zeroOdd = mapIf odd (* 0)

-- >>> zeroOdd [1,8..50]
-- [0,8,0,22,0,36,0,50]

zipWithIndex :: [a] -> [(a, Int)]
zipWithIndex xs = zip2 xs [0 ..]

-- >>> zipWithIndex "abcd"
-- [('a',0),('b',1),('c',2),('d',3)]

zeroOddIndex :: Integral a => [a] -> [a]
zeroOddIndex = map fst . mapIf (odd . snd) (\(x, i) -> (0, i)) . zipWithIndex

-- >>> zeroOddIndex [1..10]
-- [1,0,3,0,5,0,7,0,9,0]

zip3Sum :: Integral b => [b] -> [b] -> [b] -> [b]
zip3Sum xs ys zs = map (\(x, y, z) -> x + y + z) (zip3 xs ys zs)

-- >>> zip3Sum [1..10] [1..10] [1..10]
-- [3,6,9,12,15,18,21,24,27,30]

data LogLevel = Error | Warning | Info
  deriving (Eq, Show)

cmp :: LogLevel -> LogLevel -> Ordering
cmp l1 l2
  | l1 == l2 = EQ
  | l1 == Error = GT
  | l1 == Info = LT
cmp Warning l2 = if l2 == Error then LT else GT

-- >>> cmp Info Error
-- LT

instance Ord LogLevel where
  compare = cmp

-- >>> maximum [Warning, Error, Info]
-- Error

data Person = Person {firstName :: String, lastName :: String, age :: Int}
  deriving (Show)

abbrFirstName :: Person -> Person
abbrFirstName p = p {firstName = abbr $ firstName p}
  where
    abbr str =
      if length str > 1
        then take 1 str ++ "."
        else str

-- >>> abbrFirstName (Person "John" "Doe" 34)
-- Person {firstName = "J.", lastName = "Doe", age = 34}

data Tree a = Leaf | Node (Tree a) a (Tree a)
  deriving (Show)

tr0 :: Tree a
tr0 = Leaf

tr1 :: Tree Integer
tr1 = Node Leaf 12 Leaf

tr2 :: Tree Integer
tr2 = Node (Node Leaf 5 Leaf) 12 (Node Leaf 23 Leaf)

tr3 :: Tree Integer
tr3 = Node (Node Leaf 5 Leaf) 12 (Node (Node Leaf 4 Leaf) 23 Leaf)

trees :: [Tree Integer]
trees = [tr0, tr1, tr2, tr3]

treeSum :: Tree Integer -> Integer
treeSum Leaf = 0
treeSum (Node lt value rt) = treeSum lt + value + treeSum rt

-- >>> map treeSum trees
-- [0,12,40,44]

treeHeight :: Tree a -> Int
treeHeight Leaf = 0
treeHeight (Node lt _ rt) = max (treeHeight lt) (treeHeight rt) + 1

-- >>> map treeHeight trees
-- [0,1,2,3]

digits :: Integer -> [Integer]
digits = helper [] . abs
  where
    helper ds x
      | x > 0 = helper (rem x 10 : ds) (quot x 10)
      | otherwise = ds

-- >>> digits 5435
-- [5,4,3,5]


sublist :: Int -> Int -> [a] -> [a]
sublist n k | n < 0 = error "'n' shoud be greater than 0!"
            | k < n = error "'k' shoud be greater than 'n'!"
            | otherwise = take (k - n) . drop n

-- >>> sublist 2 16 [0..]
-- [2,3,4,5,6,7,8,9,10,11,12,13,14,15]


repeatEveryElem :: Int -> [a] -> [a]
repeatEveryElem n xs = [xr | x <- xs, xr <- replicate n x]

-- >>> repeatEveryElem 2 "abc"
-- "aabbcc"


movingLists :: Int -> [a] -> [[a]]
movingLists n xs = [take n . drop idx $ xs | idx <- [0..]]

-- >>> take 5 (movingLists 3 [5..7])
-- [[5,6,7],[6,7],[7],[],[]]

sum2 :: Num a => [a] -> [a] -> [a]
sum2 [] [] = []
sum2 (x:xs) [] = x : sum2 xs []
sum2 [] (y:ys) = y : sum2 [] ys
sum2 (x:xs) (y:ys) = (x + y) : sum2 xs ys

-- >>> sum2 [0..5] [10..13]
-- [10,12,14,16,4,5]


sum3 :: Num a => [a] -> [a] -> [a] -> [a]
sum3 xs ys zs = sum2 xs (sum2 ys zs)
-- >>> sum3 [0..5] [10..13] [100..110]
-- [110,113,116,119,108,110,106,107,108,109,110]


containsAllDigits :: Integer -> Bool
containsAllDigits n = all (\x -> x `elem` digits n) [1..9]
-- >>> containsAllDigits 1234567890
-- True

containsAllDigitsOnes :: Integer -> Bool
containsAllDigitsOnes n = onlyDigits && containsOnce
  where
    ds = digits n
    ints = [1..9]
    onlyDigits = null (ds \\ ints)
    containsOnce = all (\x -> length (elemIndices x ds) == 1) ints

-- >>> containsAllDigitsOnes 912345678
-- True

