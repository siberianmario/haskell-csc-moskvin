{-# LANGUAGE FlexibleContexts #-}

module Fp12 where

import Control.Monad.Except
import Data.Bifunctor
import Data.Function
import Data.List
import Data.Maybe

infixl 2 :@

infixr 3 :->

type Symb = String

-- Терм
data Expr
  = Var Symb
  | Expr :@ Expr
  | Lam Symb Expr
  deriving (Eq, Show)

-- Тип
data Type
  = TVar Symb
  | Type :-> Type
  deriving (Eq, Show)

-- Контекст
newtype Env = Env [(Symb, Type)]
  deriving (Eq, Show)

-- Подстановка
newtype SubsTy = SubsTy [(Symb, Type)]
  deriving (Eq, Show)

inferType :: Expr -> Either String Type
inferType = undefined

--------------------------------------

freeVars :: Expr -> [Symb]
freeVars (Var v) = [v]
freeVars (le :@ re) = freeVars le `union` freeVars re
freeVars (Lam v xpr) = freeVars xpr \\ [v]

freeTVars :: Type -> [Symb]
freeTVars (TVar t) = [t]
freeTVars (lt :-> rt) = freeTVars lt `union` freeTVars rt

extendEnv :: Env -> Symb -> Type -> Env
extendEnv (Env e) s t = Env $ (s, t) : e

freeTVarsEnv :: Env -> [Symb]
freeTVarsEnv (Env e) = foldl' union [] $ freeTVars . snd <$> e

appEnv :: (MonadError String m) => Env -> Symb -> m Type
appEnv (Env xs) v = maybe (err v) return (lookup v xs)
  where
    err x = throwError $ "There is no variable \"" ++ x ++ "\" in the enviroment."

tx :: Type
tx = (TVar "a" :-> TVar "b") :-> TVar "c"

ty :: Type
ty = TVar "a" :-> TVar "b"

env :: Env
env = Env [("y", ty), ("x", tx)]

-- >>> let Right res = appEnv env "x" in res
-- >>> let Right res = appEnv env "y" in res
-- >>> let Left res = appEnv env "z" in res
-- (TVar "a" :-> TVar "b") :-> TVar "c"
-- TVar "a" :-> TVar "b"
-- "There is no variable \"z\" in the enviroment."

appSubsTy :: SubsTy -> Type -> Type
appSubsTy (SubsTy subs) t@(TVar smb) = fromMaybe t $ lookup smb subs
appSubsTy s (lt :-> rt) = appSubsTy s lt :-> appSubsTy s rt

appSubsEnv :: SubsTy -> Env -> Env
appSubsEnv s (Env e) = Env $ second (appSubsTy s) <$> e

composeSubsTy :: SubsTy -> SubsTy -> SubsTy
composeSubsTy sf@(SubsTy f) (SubsTy g) = SubsTy $ unionBy eqSymb g' f
  where
    g' = second (appSubsTy sf) <$> g
    eqSymb = (==) `on` fst

instance Semigroup SubsTy where
  (<>) = composeSubsTy

instance Monoid SubsTy where
  mempty = SubsTy []

s :: SubsTy
s = SubsTy [("a", TVar "y" :-> TVar "b"), ("b", TVar "a" :-> TVar "y")] -- [a:=b→y,b:=a→y]

t :: SubsTy
t = SubsTy [("a", TVar "b" :-> TVar "y"), ("y", TVar "b")] -- [a:=b→y,c:=b]

-- T◦S=[α:=β→β, β:=(β→γ)→β, γ:=β]
-- >>> t <> s
-- SubsTy [("a",TVar "b" :-> TVar "b"),("b",(TVar "b" :-> TVar "y") :-> TVar "b"),("y",TVar "b")]

unify :: (MonadError String m) => Type -> Type -> m SubsTy
unify (TVar a) (TVar b) | a == b = return $ SubsTy []
unify ta@(TVar a) tb
  | a `elem` freeTVars tb = throwError $ "Can't unify " ++ show ta ++ " with " ++ show tb ++ "!"
  | otherwise = return $ SubsTy [(a, tb)]
unify ta@(ta1 :-> ta2) tb@(TVar b) = unify tb ta
unify ta@(ta1 :-> ta2) tb@(tb1 :-> tb2) = do
  subs2 <- unify ta2 tb2
  subs1 <- unify (appSubsTy subs2 ta1) (appSubsTy subs2 tb1)
  return $ subs1 `composeSubsTy` subs2

-- λxy.y(λz.yx)
f :: (p -> t) -> ((p -> t) -> t) -> t
f x y = y(\z -> y x)

term :: Expr
term = Lam "x" (Lam "y" (Var "y" :@ Lam "z" (Var "x")))

eqs :: Either String [(Type, Type)]
eqs = equations (Env []) term (TVar "o")

solveEqs :: (MonadError String m) => [(Type, Type)] -> m SubsTy
solveEqs (heq: teq) = snd <$> foldM folding (heq, SubsTy []) teq
  where
    folding ((ta1, ta2), subs) (tb1, tb2) = do
      sub <- unify (appSubsTy subs ta1 :-> appSubsTy subs tb1) (appSubsTy subs ta2 :-> appSubsTy subs tb2)
      let comp = sub `composeSubsTy` subs
      return ((tb1, tb2), comp)


solveEqs' :: (MonadError String m) => [(Type, Type)] -> m SubsTy
solveEqs' [(ta1, ta2), (tb1, tb2)] = unify (ta1 :-> tb1) (ta2 :-> tb2)
solveEqs' ((ta1, ta2): (tb1, tb2): tail) = do
  sub <- unify (ta1 :-> tb1) (ta2 :-> tb2)
  let newTail = bimap (appSubsTy sub) (appSubsTy sub) <$> ((ta2, tb2): tail)
  solveEqs' newTail

-- >>> eqs >>= solveEqs
-- Left "Can't unify TVar \"c\" with (TVar \"c\" :-> TVar \"d\") :-> TVar \"d\"!"

-- >>> eqs
-- Right [
--     (TVar "a" :-> TVar "b",TVar "o")
--    ,(TVar "c" :-> TVar "d",TVar "b")
--    ,(TVar "b" :-> TVar "d",TVar "c")
--    ,(TVar "d" :-> TVar "e",TVar "b")
--    ,(TVar "e",TVar "a")]

--     (TVar "a" :-> TVar "b",TVar "o")
--    ,(TVar "c" :-> TVar "d",TVar "b")
--    ,((TVar "c" :-> TVar "d") :-> TVar "d",TVar "c")
--    ,(TVar "d" :-> TVar "e",(TVar "c" :-> TVar "d"))
--    ,(TVar "e",TVar "a")]

-- >>> unify (((TVar "c" :-> TVar "d") :-> TVar "d") :-> (TVar "d" :-> TVar "e")) (TVar "c" :-> (TVar "c" :-> TVar "d")) :: Either String SubsTy
-- Left "Can't unify TVar \"c\" with (TVar \"c\" :-> TVar \"c\") :-> TVar \"c\"!"
subst1 :: SubsTy
subst1 = SubsTy [("b",TVar "c" :-> TVar "d"),("o",TVar "a" :-> (TVar "c" :-> TVar "d"))]
-- >>> appSubsTy subst1 (TVar "a" :-> TVar "b")
-- >>> appSubsTy subst1 (TVar "o")
-- TVar "a" :-> (TVar "c" :-> TVar "d")
-- TVar "a" :-> (TVar "c" :-> TVar "d")


-- >>> appSubsTy subst1 (TVar "c" :-> TVar "d")
-- >>> appSubsTy subst1 (TVar "b")
-- TVar "c" :-> TVar "d"
-- TVar "c" :-> TVar "d"

-- >>> appSubsTy subst1 (TVar "b" :-> TVar "d")
-- >>> appSubsTy subst1 (TVar "c")
-- (TVar "c" :-> TVar "d") :-> TVar "d"
-- TVar "c"

-- >>> unify ((TVar "a" :-> TVar "b") :-> (TVar "c" :-> TVar "d") :-> (TVar "b" :-> TVar "d")) (TVar "o" :-> TVar "b" :-> TVar "c") :: Either String SubsTy


-- E={β∼(γ→δ)→ε,β∼α→δ}
-- U(β→β,((γ→δ)→ε)→(α→δ))
-- S={α=γ→ε, β=(γ→ε)→ε, δ=ε}
-- S(E)={(γ→ε)→ε∼(γ→ε)→ε, (γ→ε)→ε∼(γ→ε)→ε}
t1 = TVar "b" :-> TVar "b"

t2 = ((TVar "y" :-> TVar "d") :-> TVar "e") :-> (TVar "a" :-> TVar "d")

-- >>> unify t1 t2 :: Either String SubsTy
-- Right (SubsTy [("b",(TVar "y" :-> TVar "e") :-> TVar "e"),("d",TVar "e"),("a",TVar "y" :-> TVar "e")])

-- >>> unify (TVar "a" :-> TVar "b") (TVar "c" :-> TVar "d") :: Either String SubsTy
-- >>> unify (TVar "a") (TVar "a" :-> TVar "a") :: Either String SubsTy
-- Right (SubsTy [("b",TVar "d"),("a",TVar "c")])
-- Left "Can't unify TVar \"a\" with TVar \"a\" :-> TVar \"a\"!"

equations :: (MonadError String m) => Env -> Expr -> Type -> m [(Type, Type)]
equations env (Var v) tpe = do
  tv <- appEnv env v
  return [(tpe, tv)]
equations env (v1 :@ v2) tpe = do
  let a = head $ nextTypes env tpe
  e1 <- equations env v1 (a :-> tpe)
  e2 <- equations env v2 a
  return $ e1 `union` e2
equations env (Lam smb v) tpe = do
  let [a, b] = take 2 $ nextTypes env tpe
  e1 <- equations (extendEnv env smb a) v b
  return $ (a :-> b, tpe) : e1

nextTypes :: Env -> Type -> [Type]
nextTypes env tpe = TVar <$> filter (`notElem` tvars) possibleTvars
  where
    tvars = freeTVarsEnv env `union` freeTVars tpe
    possibleTvars = (: []) <$> ['a' ..]

trm = Lam "y" $ Var "x"

env2 = Env [("x", TVar "a" :-> TVar "b")]

-- >>> equations env2 trm (TVar "o") :: Either String [(Type,Type)]
-- Right [(TVar "c" :-> TVar "d",TVar "o"),(TVar "d",TVar "a" :-> TVar "b")]

-- >>> equations (Env []) trm (TVar "o") :: Either String [(Type,Type)]
-- Left "There is no variable \"x\" in the enviroment."



-------------------------------------------------------------------

-- y:α |- (λx.xy):(α→β)→β
term2 = Lam "x" (Var "x" :@ Var "y")

eqs2 :: Either String [(Type, Type)]
eqs2 = equations (Env [("y", TVar "a")]) term2 (TVar "o")

-- >>> eqs2
-- Right [
--  (TVar "b" :-> TVar "c",TVar "o"),
--  (TVar "d" :-> TVar "c",TVar "b"),
--  (TVar "d",TVar "a")]

--  (TVar "a" :-> TVar "b",TVar "o"),
--  (TVar "d" :-> TVar "b",TVar "a")

-- >>> unify ((TVar "a" :-> TVar "b") :-> (TVar "d" :-> TVar "b")) (TVar "o" :-> TVar "a") :: Either String SubsTy
-- Right (SubsTy [("a",TVar "d" :-> TVar "b"),("o",(TVar "d" :-> TVar "b") :-> TVar "b")])

-- >>> eqs2 >>= solveEqs
-- Right (SubsTy [("b",TVar "a" :-> TVar "c"),("o",(TVar "a" :-> TVar "c") :-> TVar "c"),("d",TVar "a")])


-------------------------------------------------------------------

-- (λxy.yx):α→(α→β)→β
f3 :: t1 -> (t1 -> t2) -> t2
f3 x y = y x

term3 :: Expr
term3 = Lam "x" (Lam "y" (Var "x" :@ Var "y"))

eqs3 :: Either String [(Type, Type)]
eqs3 = equations (Env []) term3 (TVar "o")

-- >>> eqs3
-- (TVar "a" :-> TVar "b",TVar "o"),
-- (TVar "c" :-> TVar "d",TVar "b"),
-- (TVar "b" :-> TVar "d",TVar "a"),
-- (TVar "b",TVar "c")]


-- ((TVar "c" :-> TVar "d") :-> TVar "d",TVar "a"),
-- (TVar "c" :-> TVar "d",TVar "c")]


-- ((TVar "c" :-> TVar "d") :-> TVar "c",TVar "o"),
-- (TVar "c" :-> TVar "d",TVar "c")

-- >>> unify ((TVar "a" :-> TVar "b") :-> (TVar "c" :-> TVar "d")) ((TVar "o") :-> (TVar "b")) :: Either String SubsTy
-- Right (SubsTy [("b",TVar "c" :-> TVar "d"),("o",TVar "a" :-> (TVar "c" :-> TVar "d"))])

-- >>> unify ((TVar "a" :-> TVar "b") :-> (TVar "c" :-> TVar "d")) ((TVar "o") :-> (TVar "b")) :: Either String SubsTy
-- Left "Can't unify TVar \"c\" with TVar \"c\" :-> TVar \"d\"!"

-- >>> eqs3 >>= solveEqs
-- Left "Can't unify TVar \"c\" with TVar \"c\" :-> TVar \"d\"!"
