{-# LANGUAGE StandaloneDeriving #-}

module Fp06 where

import Data.Complex ( Complex(..) )

-- Производные представители (Derived Instances)
data Point a = Point a a deriving (Eq)

-- >>> Point 3 5 == Point 3 5
-- True

-- В GHC можно и отдельно (с ключом -XStandaloneDeriving)
deriving instance Show a => Show (Point a)

-- вспомогательная эмуляция низкоуровневой реализации
eqInt :: Int -> Int -> Bool
eqInt x y = x == y

-- словарь для Eq', то есть запись из его методов
data Eq' a = MkEq (a -> a -> Bool) (a -> a -> Bool)

-- функции-селекторы выбирают методы равенства и неравенства из этого словаря
eq (MkEq e _) = e

ne (MkEq _ n) = n

-- объявления instance транслируются в функции, возвращающие словарь...
dEqInt :: Eq' Int
dEqInt = MkEq eqInt (\x y -> not $ eqInt x y)

-- ... или в функции, принимающие некоторый словарь и возвращающие более сложный словарь
dEqList :: Eq' a -> Eq' [a]
dEqList (MkEq e _) = MkEq el (\x y -> not $ el x y)
  where
    el [] [] = True
    el (x : xs) (y : ys) = x `e` y && xs `el` ys
    el _ _ = False

-- Функция elem теперь принимает словарь в качестве явного параметра
elem' :: Eq' a -> a -> [a] -> Bool
elem' _ _ [] = False
elem' d x (y : ys) = eq d x y || elem' d x ys

data Tree a = Leaf a | Branch (Tree a) a (Tree a)
  deriving (Eq)

tree :: Tree Integer
tree = Branch (Leaf 1) 2 (Branch (Leaf 3) 4 (Leaf 5))

infTree :: Num a => a -> Tree a
infTree a = Branch (infTree (a - 1)) a (infTree (a + 1))

elemTree :: Eq a => a -> Tree a -> Bool
elemTree x (Leaf a) = a == x
elemTree x (Branch lt a rt) = (a == x) || elemTree x lt || elemTree x rt

-- >>> elemTree (-100) (infTree 0)
-- True

instance Functor Tree where
  fmap f (Leaf a) = Leaf (f a)
  fmap f (Branch lt a rt) = Branch (fmap f lt) (f a) (fmap f rt)

-- >>> fmap (*2) tree
-- <2{4}<6{8}10>>

data List a = Nil | Cons a (List a)

list :: List Integer
list = Cons 2 (Cons 3 (Cons 5 Nil))

-- >>> list
-- <2<3<5|>>>

instance Show a => Show (List a) where
  showsPrec _ = showsList

showsList :: Show a => List a -> ShowS
showsList Nil = showString "|"
showsList (Cons x xs) = showChar '<' . shows x . showsList xs . showChar '>'

instance Show a => Show (Tree a) where
  -- show = showTree
  showsPrec _ = showsTree

showTree :: Show a => Tree a -> String
showTree (Leaf a) = show a
showTree (Branch lt a rt) =
  "<" ++ show lt
    ++ "{"
    ++ show a
    ++ "}"
    ++ show rt
    ++ ">"

showsTree :: Show a => Tree a -> ShowS
showsTree (Leaf a) = shows a
showsTree (Branch lt a rt) =
  showChar '<'
    . showsTree lt
    . showChar '{'
    . shows a
    . showChar '}'
    . showsTree rt
    . showChar '>'

-- >>> tree
-- <1{2}<3{4}5>>

-- >>> (reads "54 golden rings") :: [(Integer,String)]
-- [(54," golden rings")]

instance Read a => Read (List a) where
  readsPrec _ = myReadsList

myReadsList :: (Read a) => ReadS (List a)
myReadsList ('|' : s) = [(Nil, s)]
myReadsList ('<' : s) = [(Cons x l, u) | (x, t)       <- reads s,
                                         (l, '>' : u) <- myReadsList t]

-- >>> myReadsList "<2<3<5|>>>>> something else" :: [(List Int, String)]
-- [(<2<3<5|>>>,">> something else")]


myReadsTree :: (Read a) => ReadS (Tree a)
myReadsTree ('<' : s) = [(Branch lt a rt, u)  | (lt, '{' : t1) <- myReadsTree s,
                                                (a, '}' : t2) <- reads t1,
                                                (rt, '>' : u) <- myReadsTree t2]
myReadsTree s = [(Leaf a, t) | (a, t) <- reads s]

-- >>> (myReadsTree "<1{2}<3{4}5>> something else") :: [(Tree Int, String)]
-- [(<1{2}<3{4}5>>," something else")]

newtype Matrix a = Matrix [[a]]
  -- deriving Show

instance Show a => Show (Matrix a) where
  showsPrec _ = showsMatrix

showsMatrix :: Show a => Matrix a -> ShowS
showsMatrix (Matrix []) = showString "EMPTY"
showsMatrix (Matrix [[]]) = showString "EMPTY"
showsMatrix (Matrix m) = foldr (\l ss -> showList l . showChar '\n' . ss) (showString "") m

-- >>> Matrix [] :: Matrix Int
-- EMPTY

-- >>> Matrix [[1,2,3],[4,5,6],[7,8,9]]
-- [1,2,3]
-- [4,5,6]
-- [7,8,9]


newtype Cmplx = Cmplx (Complex Double) deriving Eq

z :: Cmplx
z = Cmplx $ (-2.7) :+ 3.4

instance Show Cmplx where
  show (Cmplx (r :+ i)) = show r ++ sign i ++ "i*" ++ show (abs i)
    where
      sign d = if signum d < 0 then "-" else "+"

instance Read Cmplx where
  readsPrec _ = readsCmplx

readsCmplx :: ReadS Cmplx
readsCmplx s = [(Cmplx (r :+ (unsign sign * abs i)), u) | (r, sign : 'i' : '*' : t) <- reads s,
                                                          (i, u) <- reads t]
  where unsign '-' = -1
        unsign '+' = 1
        unsign _ = error "sign char should be '+' or '-'"

-- >>> read (show z) == z
-- True

