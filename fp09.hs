module Fp09 where

--import Control.Monad.Instances
import Control.Applicative
import Control.Monad.Identity
import Data.Functor (($>))

do1 :: Maybe Bool
do1 = do
  x <- Just 17
  return (x < 21)

do2 :: [Integer]
do2 = do
  x <- [1, 2, 3]
  [x, x * 10]

do3 :: [(Integer, Char)]
do3 = do
  n <- [1, 2]
  c <- ['a', 'b']
  return (n, c)

filter' :: (a -> Bool) -> [a] -> [a]
filter' p xs = do
  x <- xs
  True <- return (p x)
  return x

-- >>> filter' odd [1..10]
-- [1,3,5,7,9]

replicate' :: Int -> a -> [a]
replicate' n x = do
  take n [0 ..]
  return x

-- >>> replicate' 5 'a'
-- "aaaaa"

($>.) :: Monad m => m a -> b -> m b
xs $>. y = xs >> return y

-- >>> [1,2,3] $>. 'a'
-- "aaa"

fmap' :: Monad m => (a -> b) -> m a -> m b
fmap' f xs = do
  f <$> xs

-- >>> fmap' id (Just 5)
-- >>> fmap' ((+3) . (*2)) (Just 5)
-- >>> fmap' (+3) . fmap' (*2) $ Just 5
-- Just 5
-- Just 13
-- Just 13

(*>.) :: Monad m => m a -> m b -> m b
xs *>. ys = xs >> ys

-- >>> [1,2,3] *>. "ab"
-- "ababab"

liftA2' :: Monad m => (a -> b -> c) -> m a -> m b -> m c
liftA2' f xs ys = do
  x <- xs
  f x <$> ys

-- >>> liftA2' (+) [10,20] [1,2]
-- [11,12,21,22]

(<*>.) :: (Monad m) => m (a -> b) -> m a -> m b
fs <*>. xs = fs >>= \f -> xs >>= \x -> return (f x)

-- identity
-- >>> [id] <*>. [1, 2, 3] == [1,2,3]
-- True

-- composition:
-- >>> [(.)] <*>. [(+2)] <*>. [(*3)] <*>. [1, 2, 3] == [(+2)] <*>. ([(*3)] <*>. [1, 2, 3])
-- True

-- homomorphism:
-- >>> [(+2)] <*>. [1, 2, 3] == [3, 4, 5]
-- True

-- interchange:
-- >>> Just (+2) <*>. Just 5 == Just ($ 5) <*>. Just (+2)
-- True

(>=>.) :: Monad m => (a -> m b) -> (b -> m c) -> (a -> m c)
f >=>. g = \a -> f a >>= \b -> g b

-- >>> (\x -> [x,x+10]) >=> (\x -> [x,2*x]) $ 1
-- >>> (\x -> [x,x+10]) >=>. (\x -> [x,2*x]) $ 1
-- [1,2,11,22]
-- [1,2,11,22]

join' :: (Monad m) => m (m a) -> m a
join' mma = mma >>= id

-- >>> join ["aaa","bb"]
-- >>> join' ["aaa","bb"]
-- "aaabb"
-- "aaabb"

isTiny :: (Ord a, Num a) => a -> Bool
isTiny x = x >= (-128) && x < 128

(?+?) :: (Ord a, Num a) => a -> a -> Maybe a
(?+?) x y = let sum = x + y in if isTiny sum then Just sum else Nothing

-- >>> 3 ?+? 100
-- >>> 90 ?+? 100
-- Just 103
-- Nothing

sumTiny :: (Foldable t, Ord b, Num b) => t b -> Maybe b
sumTiny = foldM (?+?) 0

-- >>> sumTiny [1..15]
-- >>> sumTiny [1..16]
-- Just 120
-- Nothing

type IntBoard = Int

brd :: IntBoard
brd = 5

next :: IntBoard -> [IntBoard]
next ini = filter (>= 0) . filter (<= 9) $ [ini + 2, ini -1]

twoTurns :: IntBoard -> [IntBoard]
twoTurns ini = do
  bd1 <- next ini
  next bd1

threeTurns :: IntBoard -> [IntBoard]
threeTurns ini = do
  bd1 <- next ini
  bd2 <- next bd1
  next bd2

doNTurns :: Int -> IntBoard -> [IntBoard]
doNTurns n ini = foldM (\i _ -> next i) ini (take n [0 ..])

doNTurns' :: Int -> IntBoard -> [IntBoard]
doNTurns' n = foldl (\k _ -> k >=> next) (: []) (take n [0 ..])

-- >>> next brd
-- >>> twoTurns brd
-- >>> threeTurns brd
-- >>> doNTurns 1 brd
-- >>> doNTurns 2 brd
-- >>> doNTurns 3 brd
-- >>> doNTurns' 1 brd
-- >>> doNTurns' 2 brd
-- >>> doNTurns' 3 brd
-- [7,4]
-- [9,6,6,3]
-- [8,8,5,8,5,5,2]
-- [7,4]
-- [9,6,6,3]
-- [8,8,5,8,5,5,2]
-- [7,4]
-- [9,6,6,3]
-- [8,8,5,8,5,5,2]

(>=>..) :: Monad m => (a -> m b) -> (b -> m c) -> a -> m c
f >=>.. g = (g =<<) . f

-- join . return      ==  id
-- join . fmap return ==  id
-- join . join == join . fmap join

surround :: a -> a -> [a] -> [a]
surround x y zs = do
  z <- zs
  [x, z, y]

-- >>> surround '{' '}' "abcd"
-- "{a}{b}{c}{d}"

lookups :: (Eq a) => a -> [(a, b)] -> [b]
lookups x ys = do
  (k, v) <- ys
  True <- return (k == x)
  return v

-- >>> lookups 2 [(1,"one"),(2,"two"),(3,"three"),(2,"two'")]
-- ["two","two'"]

factor2 :: Integer -> [(Integer, Integer)]
factor2 n = do
  i <- [1 .. (floor . sqrt . fromInteger $ n)]
  let (d, m) = n `divMod` i
  True <- return (m == 0)
  return (i, d)

-- >>> factor2 45
-- [(1,45),(3,15),(5,9)]

absDiff :: Num a => [a] -> [a]
absDiff xs = do
  let ps = zip xs (tail xs)
  (x, y) <- ps
  return (abs (x - y))

-- >>> absDiff [2,7,22,9]
-- [5,15,13]

data Cell
  = E -- empty, пустая клетка
  | X -- крестик
  | O -- нолик
  deriving (Eq, Show)

type Row a = [a]

type Board = Row (Row Cell)

iniBoard :: Int -> Board
iniBoard n = let row = replicate n E in replicate n row

checkRow :: Cell -> Row Cell -> Bool
checkRow _ [] = False
checkRow x row = all (== x) row

checkRows :: Cell -> Board -> Bool
checkRows x = any (checkRow x)

checkColumns :: Cell -> Board -> Bool
checkColumns _ [] = False
checkColumns _ ([] : _) = False
checkColumns x brd = checkRow x (map head brd) || checkColumns x (map tail brd)

checkDiags :: Cell -> Board -> Bool
checkDiags x brd = checkRow x frontDiag || checkRow x backDiag
  where
    len = length brd
    frontDiag = zipWith (!!) brd [0 ..]
    backDiag = zipWith (!!) brd [len - 1, len - 2 .. 0]

win :: Cell -> Board -> Bool
win E _ = False
win x brd = checkRows x brd || checkColumns x brd || checkDiags x brd

opponent :: Cell -> Cell
opponent E = E
opponent X = O
opponent O = X

-- >>> win X [[X,O,X],[O,X,O],[X,O,X]]
-- True

replace :: a -> Int -> [a] -> [a]
replace a i list = let (hs, _ : ts) = splitAt i list in hs ++ a : ts

move :: Cell -> Int -> Int -> Board -> Board
move x r c brd = replace (replace x c (brd !! r)) r brd

nxt :: Board -> Cell -> [Board]
nxt brd x = do
  False <- return (win (opponent x) brd)
  (r, row) <- zip [0 ..] brd
  (c, cell) <- zip [0 ..] row
  True <- return (cell == E)
  return (move x r c brd)

-- >>> nxt X (iniBoard 2)
-- [[[X,E],[E,E]],[[E,X],[E,E]],[[E,E],[X,E]],[[E,E],[E,X]]]

goNTurns :: Int -> Board -> [Board]
goNTurns n ini = foldM nxt ini turns
  where
    turns = take n . concat . repeat $ [X, O]

-- >>> length $ goNTurns 5 (iniBoard 3)
-- 15120
