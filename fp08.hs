{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE FlexibleContexts #-}

module Fp08 where

import Control.Applicative ((<*>), (*>), (<$>), (<*), ZipList(ZipList, getZipList))
import Data.Functor ((<$>))
import Data.Traversable ()
import Data.Foldable (fold, foldl)

import Text.Parsec
import Data.Functor.Identity (Identity)

{-
class Functor f => Pointed f where
  pure :: a -> f a -- aka singleton, return, unit, point

instance Pointed Maybe where
--pure :: a -> Maybe a
  pure x  = Just x

instance Pointed [] where
--pure :: a -> [a]
  pure x  = [x]

instance Pointed ((->) e) where
--pure :: a -> e -> a
  pure = const

-- не очень-то хорошо, контекст Monoid s => может спасти дело
instance Pointed ((,) s) where
--pure :: a -> (s, a)
  pure x = (undefined, x)

instance Pointed (Either e) where
--pure :: a -> Either e a
  pure x = Right x

instance Pointed Tree where
--pure :: a -> Tree a
  pure x = Leaf x -- как для деревьев другой структуры, например с Nil?
-}

-- Аппликативный дистрибьютор списка
dist :: Applicative f => [f a] -> f [a]
dist [] = pure []
dist (ax : axs) = (:) <$> ax <*> (dist axs)

{-
*Fp08> dist [Just 3,Just 5]
Just [3,5]
*Fp08> dist [Just 3,Nothing]
Nothing
*Fp08> getZipList $ dist $ map ZipList [[1,2,3],[4,5,6]]
[[1,4],[2,5],[3,6]]
*Fp08>
-}

apArrow :: (e -> a -> b -> c) -> (e -> a) -> (e -> b) -> e -> c
apArrow f g h e = f e (g e) (h e)

-- apArrow f g h = f <*> g <*> h

apply2Arrow :: (a -> b -> c) -> (e -> a) -> (e -> b) -> e -> c
apply2Arrow f g h e = f (g e) (h e)

-- apply2Arrow f g h = f <$> g <*> h

data MyEither a b = MyLeft a | MyRight b deriving (Eq, Show)

instance Functor (MyEither e) where
  fmap f (MyLeft e) = MyLeft e
  fmap f (MyRight x) = MyRight (f x)

instance Applicative (MyEither e) where
  pure = MyRight
  (<*>) (MyLeft e) _ = MyLeft e
  (<*>) (MyRight f) g = ap f g
    where
      ap _ (MyLeft e) = MyLeft e
      ap f (MyRight x) = MyRight (f x)


prs :: Stream s Identity t => Parsec s () a -> s -> Either ParseError a
prs rule = parse rule ""

p0 :: Parsec String () (String, String)
p0 = (,) <$> many letter <*> many digit

p1 :: Parsec String () (String, String)
p1 = (,) <$> many1 letter <*> (many space *> many1 digit)

p2 :: Parsec String () String
p2 = string "ABC" <|> string "DEF"
-- >>> prs (char 'A' <* char 'B' *> char 'C') "ABC"
-- Right 'C'

-- >>> (getZipList . sequenceA . map ZipList) [[1,2,3],[4,5,6]]
-- [[1,4],[2,5],[3,6]]


newtype Cmps f g x = Cmps {getCmps :: f (g x)} deriving (Show, Eq)

instance (Functor f, Functor g) => Functor (Cmps f g) where
  fmap h = Cmps . fmap (fmap h) . getCmps

instance (Applicative f, Applicative g) => Applicative (Cmps f g) where
  pure = Cmps . pure . pure
  Cmps fgh <*> Cmps fgx = Cmps $ (<*>) <$> fgh <*> fgx

instance (Foldable f, Foldable g) => Foldable (Cmps f g) where
  foldr h z = foldr (flip (foldr h)) z . getCmps

instance (Traversable f, Traversable g) => Traversable (Cmps f g) where
  traverse h = fmap Cmps . traverse (traverse h) . getCmps

lm3 :: Cmps [] Maybe Integer
lm3 = Cmps [Just 3]

lmPlus3 :: Cmps [] Maybe (Integer -> Integer)
lmPlus3 = Cmps [Just (+3)]

lmMul2 :: Cmps [] Maybe (Integer -> Integer)
lmMul2 = Cmps [Just (*2)]

ffmap :: Functor (Cmps f g) => (a -> x) -> f (g a) -> f (g x)
ffmap h = getCmps . fmap h . Cmps

ffap :: Applicative (Cmps f g) => f (g (a -> b)) -> f (g a) -> f (g b)
ffap h x = getCmps $ Cmps h <*> Cmps x

-- >>> ffmap (+42) $ [Just 1,Just 2,Nothing]
-- [Just 43,Just 44,Nothing]

-- >>> ffap [Just (*2), Just (+3)] [Just 5, Just 12]
-- [Just 10,Just 24,Just 8,Just 15]

-- >>> foldl1 (+) (Cmps [Just 1,Just 2,Nothing])
-- 3

-- >>> traverse (\x -> if odd x then Right x else Left x) (Cmps [Just 1,Just 3,Nothing])
-- Right (Cmps {getCmps = [Just 1,Just 3,Nothing]})

-- Identity
-- >>> (pure id <*> lm3) == lm3
-- True

-- Composition
-- >>> (pure (.) <*> lmMul2 <*> lmPlus3 <*> lm3) == (lmMul2 <*> (lmPlus3 <*> lm3))
-- True

-- Homomorphism
-- >>> (lmMul2 <*> lm3) == pure (6)
-- True

-- Interchange
-- >>> (lmMul2 <*> lm3) == (pure ($ 3) <*> lmMul2)
-- True


(>$<) :: (a -> b) -> [a] -> [b]
(>$<) = (<$>)

(>*<) :: [a -> b] -> [a] -> [b]
(>*<) f a = getZipList $ ZipList f <*> ZipList a

infixl 4 >$<, >*<

x1s = [1,2,3]
x2s = [4,5,6]
x3s = [7,8,9]
x4s = [10,11,12]

-- >>> (\a b c d -> 2*a+3*b+5*c-4*d) >$< x1s >*< x2s >*< x3s >*< x4s
-- [9,15,21]


data Triple a = Tr a a a deriving (Eq,Show)

instance Functor Triple where
  fmap f (Tr x y z) = Tr (f x) (f y) (f z)

instance Applicative Triple where
  pure a = Tr a a a
  (<*>) (Tr f g h) (Tr x y z) = Tr (f x) (g y) (h z)

-- >>> Tr (^2) (+2) (*3) <*> Tr 2 3 4
-- Tr 4 5 12

instance Foldable Triple where
  foldr f ini (Tr x y z) = f x $ f y $ f z ini
-- >>> foldl (++) "!!" (Tr "ab" "cd" "efg")
-- "!!abcdefg"

instance Traversable Triple where
  traverse f (Tr x y z) = Tr <$> f x <*> f y <*> f z
-- >>> traverse (\x -> if x>10 then Right x else Left x) (Tr 12 8 4)
-- Left 8


data Tree a = Nil | Branch (Tree a) a (Tree a) deriving (Eq, Show)

t1 :: Tree Integer
t1 = Branch (Branch Nil 1 Nil) 2 Nil
t2 :: Tree Integer
t2 = Branch (Branch Nil 3 Nil) 4 (Branch Nil 5 Nil)

instance Functor Tree where
  fmap g Nil = Nil
  fmap g (Branch l x r) = Branch (fmap g l) (g x) (fmap g r)

-- >>> fmap (^2) t2
-- >>> (^3) <$> t2
-- Branch (Branch Nil 9 Nil) 16 (Branch Nil 25 Nil)
-- Branch (Branch Nil 27 Nil) 64 (Branch Nil 125 Nil)

instance Applicative Tree where
  pure x = Branch Nil x Nil
  (<*>) Nil _ = Nil
  (<*>) _ Nil = Nil
  (<*>) (Branch lf f rf) (Branch lx x rx) = Branch (lf <*> lx) (f x) (rf <*> rx)

-- >>> (*) <$> t1 <*> t2
-- Branch (Branch Nil 3 Nil) 8 Nil

instance Foldable Tree where
  foldr f z Nil = z
  foldr f z (Branch l x r) = foldr f (f x (foldr f z r)) l
-- >>> foldr (:) [] t2
-- >>> fold (Branch (Branch Nil "3" Nil) "4" (Branch Nil "5" Nil))
-- [3,4,5]
-- "345"

instance Traversable Tree where
  traverse f Nil = pure Nil
  traverse f (Branch l x r) = Branch <$> traverse f l <*> f x <*> traverse f r
-- >>> traverse (\x -> if odd x then Right x else Left x) (Branch (Branch Nil 1Nil) 3 Nil)
-- >>> traverse (\x -> if odd x then Right x else Left x) (Branch (Branch Nil 1Nil) 2 Nil)
-- Right (Branch (Branch Nil 1 Nil) 3 Nil)
-- Left 2
